package com.gaurav.camelcomponent.jetty.routes;

import org.apache.camel.builder.RouteBuilder;

public class JettyRoute extends RouteBuilder {


    @Override
    public void configure() throws Exception {
        from("jetty:http://localhost:8777")
                .to("class:com.gaurav.camelcomponent.jetty.CalledClass?method=hello");
    }
}
