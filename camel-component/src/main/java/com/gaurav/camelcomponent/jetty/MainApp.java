package com.gaurav.camelcomponent.jetty;

import com.gaurav.camelcomponent.jetty.routes.JettyRoute;
import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;

public class MainApp {

    public static void main (String[] args)
    {
        MainApp app = new MainApp();
        app.process();
    }

    private void process() {
        CamelContext ctx = new DefaultCamelContext();
        try {
            ctx.addRoutes(new JettyRoute());
            ctx.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
